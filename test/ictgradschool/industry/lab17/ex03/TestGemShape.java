package ictgradschool.industry.lab17.ex03;


import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestGemShape {
    private MockPainter painter;

    @Before
    public void setUp() {
        painter = new MockPainter();
    }


    @Test
    public void testDefaultConstructor() {
        GemShape shape = new GemShape();

        assertEquals(0, shape.getX());
        assertEquals(0, shape.getY());
        assertEquals(5, shape.getDeltaX());
        assertEquals(5, shape.getDeltaY());
        assertEquals(25, shape.getWidth());
        assertEquals(35, shape.getHeight());

        shape.paint(painter);
        assertEquals("(polygon xpoints: [0, 12, 25, 12], ypoints: [17, 0, 17, 35])", painter.toString());
    }

    @Test
    public void testConstructorWithSpeedValues() {
        GemShape shape = new GemShape(1, 2, 3, 4);

        assertEquals(1, shape.getX());
        assertEquals(2, shape.getY());
        assertEquals(3, shape.getDeltaX());
        assertEquals(4, shape.getDeltaY());
        assertEquals(25, shape.getWidth());
        assertEquals(35, shape.getHeight());

        shape.paint(painter);
        assertEquals("(polygon xpoints: [1, 13, 26, 13], ypoints: [19, 2, 19, 37])", painter.toString());
    }

    @Test
    public void testConstructorWithAllValuesWidth40Less() {
        GemShape shape = new GemShape(1, 2, 3, 4, 5, 6);
        //test shape less then 40 width.

        assertEquals(1, shape.getX());
        assertEquals(2, shape.getY());
        assertEquals(3, shape.getDeltaX());
        assertEquals(4, shape.getDeltaY());
        assertEquals(5, shape.getWidth());
        assertEquals(6, shape.getHeight());

        shape.paint(painter);
        assertEquals("(polygon xpoints: [1, 3, 6, 3], ypoints: [5, 2, 5, 8])", painter.toString());

    }

    @Test
    public void testConstructorWithAllValuesWidth40More() {
        GemShape shape = new GemShape(10, 20, 30, 40, 50, 60);
        //test shape more than 40 width.

        assertEquals(10, shape.getX());
        assertEquals(20, shape.getY());
        assertEquals(30, shape.getDeltaX());
        assertEquals(40, shape.getDeltaY());
        assertEquals(50, shape.getWidth());
        assertEquals(60, shape.getHeight());

        shape.paint(painter);

        assertEquals("(polygon xpoints: [10, 30, 40, 60, 40, 30, 0, 0], ypoints: [50, 20, 20, 50, 80, 80, 0, 0])", painter.toString());
    }

    @Test
    public void testSimpleMoveWidth40less() {
        GemShape shape = new GemShape(10, 10, 12, 15, 20, 20);
        shape.paint(painter);
        shape.move(500, 500);
        shape.paint(painter);

        assertEquals("(polygon xpoints: [10, 20, 30, 20], ypoints: [20, 10, 20, 30])(polygon xpoints: [22, 32, 42, 32], ypoints: [35, 25, 35, 45])", painter.toString());

    }

    @Test
    public void testSimpleMoveWidth40More() {
        GemShape shape = new GemShape(10, 10, 12, 15, 45, 45);
        shape.paint(painter);
        shape.move(500, 500);
        shape.paint(painter);

        assertEquals("(polygon xpoints: [10, 30, 35, 55, 35, 30, 0, 0], ypoints: [32, 10, 10, 32, 55, 55, 0, 0])(polygon xpoints: [22, 42, 47, 67, 47, 42, 0, 0], ypoints: [47, 25, 25, 47, 70, 70, 0, 0])", painter.toString());

    }

    @Test
    public void testShapeMoveWithBounceOffRight40less() {
        GemShape shape = new GemShape(100, 20, 12, 15, 20, 20);
        shape.paint(painter);
        shape.move(135, 10000);
        shape.paint(painter);
        shape.move(135, 10000);
        shape.paint(painter);

        assertEquals("(polygon xpoints: [100, 110, 120, 110], ypoints: [30, 20, 30, 40])(polygon xpoints: [112, 122, 132, 122], ypoints: [45, 35, 45, 55])(polygon xpoints: [115, 125, 135, 125], ypoints: [60, 50, 60, 70])", painter.toString());
    }

    @Test
    public void testShapeMoveWithBounceOffRight40More() {
        GemShape shape = new GemShape(100, 20, 12, 15, 50, 50);
        shape.paint(painter);
        shape.move(135, 10000);
        shape.paint(painter);
        shape.move(135, 10000);
        shape.paint(painter);

        assertEquals("(polygon xpoints: [100, 120, 130, 150, 130, 120, 0, 0], ypoints: [45, 20, 20, 45, 70, 70, 0, 0])(polygon xpoints: [85, 105, 115, 135, 115, 105, 0, 0], ypoints: [60, 35, 35, 60, 85, 85, 0, 0])(polygon xpoints: [73, 93, 103, 123, 103, 93, 0, 0], ypoints: [75, 50, 50, 75, 100, 100, 0, 0])", painter.toString());
    }

    @Test
    public void testShapeMoveWithBounceOffLeft40less() {
        GemShape shape = new GemShape(100, 20, -12, 15, 25, 25);
        shape.paint(painter);
        shape.move(10000, 10000);
        shape.paint(painter);
        shape.move(10000, 10000);
        shape.paint(painter);
        assertEquals("(polygon xpoints: [100, 112, 125, 112], ypoints: [32, 20, 32, 45])(polygon xpoints: [88, 100, 113, 100], ypoints: [47, 35, 47, 60])(polygon xpoints: [76, 88, 101, 88], ypoints: [62, 50, 62, 75])", painter.toString());
    }

    @Test
    public void testShapeMoveWithBounceOffLeft40More() {
        GemShape shape = new GemShape(100, 20, -12, 15, 50, 80);
        shape.paint(painter);
        shape.move(10000, 10000);
        shape.paint(painter);
        shape.move(10000, 10000);
        shape.paint(painter);
        assertEquals("(polygon xpoints: [100, 120, 130, 150, 130, 120, 0, 0], ypoints: [60, 20, 20, 60, 100, 100, 0, 0])(polygon xpoints: [88, 108, 118, 138, 118, 108, 0, 0], ypoints: [75, 35, 35, 75, 115, 115, 0, 0])(polygon xpoints: [76, 96, 106, 126, 106, 96, 0, 0], ypoints: [90, 50, 50, 90, 130, 130, 0, 0])", painter.toString());
    }

    @Test
    public void testShapeMoveWithBounceOffTop40less() {
        GemShape shape = new GemShape(100, 20, 12, -15, 25, 25);
        shape.paint(painter);
        shape.move(10000, 10000);
        shape.paint(painter);
        shape.move(10000, 10000);
        shape.paint(painter);
        assertEquals("(polygon xpoints: [100, 112, 125, 112], ypoints: [32, 20, 32, 45])(polygon xpoints: [112, 124, 137, 124], ypoints: [17, 5, 17, 30])(polygon xpoints: [124, 136, 149, 136], ypoints: [12, 0, 12, 25])", painter.toString());
    }

    @Test
    public void testShapeMoveWithBounceOffTop40More() {
        GemShape shape = new GemShape(100, 20, 12, -15, 50, 70);
        shape.paint(painter);
        shape.move(10000, 10000);
        shape.paint(painter);
        shape.move(10000, 10000);
        shape.paint(painter);
        assertEquals("(polygon xpoints: [100, 120, 130, 150, 130, 120, 0, 0], ypoints: [55, 20, 20, 55, 90, 90, 0, 0])(polygon xpoints: [112, 132, 142, 162, 142, 132, 0, 0], ypoints: [40, 5, 5, 40, 75, 75, 0, 0])(polygon xpoints: [124, 144, 154, 174, 154, 144, 0, 0], ypoints: [35, 0, 0, 35, 70, 70, 0, 0])", painter.toString());
    }

    @Test
    public void testShapeMoveWithBounceOffBottom40less() {
        GemShape shape = new GemShape(10, 20, 0, 15);
        shape.paint(painter);
        shape.move(10000, 60);
        shape.paint(painter);
        shape.move(10000, 60);
        shape.paint(painter);
        assertEquals("(polygon xpoints: [10, 22, 35, 22], ypoints: [37, 20, 37, 55])(polygon xpoints: [10, 22, 35, 22], ypoints: [42, 25, 42, 60])(polygon xpoints: [10, 22, 35, 22], ypoints: [27, 10, 27, 45])", painter.toString());
    }

    @Test
    public void testShapeMoveWithBounceOffBottom40More() {
        GemShape shape = new GemShape(10, 20, 0, 15,50,60);
        shape.paint(painter);
        shape.move(10000, 60);
        shape.paint(painter);
        shape.move(10000, 60);
        shape.paint(painter);
        assertEquals("(polygon xpoints: [10, 30, 40, 60, 40, 30, 0, 0], ypoints: [50, 20, 20, 50, 80, 80, 0, 0])(polygon xpoints: [10, 30, 40, 60, 40, 30, 0, 0], ypoints: [30, 0, 0, 30, 60, 60, 0, 0])(polygon xpoints: [10, 30, 40, 60, 40, 30, 0, 0], ypoints: [30, 0, 0, 30, 60, 60, 0, 0])", painter.toString());
    }

    @Test
    public void testShapeMoveWithBounceOffBottomAndLeft40less() {
        GemShape shape = new GemShape(10, 90, -12, 15);
        shape.paint(painter);
        shape.move(125, 135);
        shape.paint(painter);
        shape.move(125, 135);
        shape.paint(painter);
        assertEquals("(polygon xpoints: [10, 22, 35, 22], ypoints: [107, 90, 107, 125])(polygon xpoints: [0, 12, 25, 12], ypoints: [117, 100, 117, 135])(polygon xpoints: [12, 24, 37, 24], ypoints: [102, 85, 102, 120])", painter.toString());
    }

    @Test
    public void testShapeMoveWithBounceOffBottomAndLeft40More() {
        GemShape shape = new GemShape(10, 90, -12, 15,50,60);
        shape.paint(painter);
        shape.move(125, 135);
        shape.paint(painter);
        shape.move(125, 135);
        shape.paint(painter);
        assertEquals("(polygon xpoints: [10, 30, 40, 60, 40, 30, 0, 0], ypoints: [120, 90, 90, 120, 150, 150, 0, 0])(polygon xpoints: [0, 20, 30, 50, 30, 20, 0, 0], ypoints: [105, 75, 75, 105, 135, 135, 0, 0])(polygon xpoints: [12, 32, 42, 62, 42, 32, 0, 0], ypoints: [90, 60, 60, 90, 120, 120, 0, 0])", painter.toString());
    }


    //testShapeMoveWithBounceOffBottomAndRight - less than 40 and more than 40
    //testShapeMoveWithBounceOffTopAndRight - less than 40 and more than 40
    //testShapeMoveWithBounceOffTopAndLeft - less than 40 and more than 40
    //all the same as the top method. Save time not to do this.


}
