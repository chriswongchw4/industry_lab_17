package ictgradschool.industry.lab17.ex03;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        TestNestingShape.class,
        TestRectangleShape.class,
        TestOvalShape.class,
        TestGemShape.class

})


public class TestAll {
}
