package ictgradschool.industry.lab17.ex02;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

/**
 * TODO Implement this class.
 */
public class TestSimpleSpellChecker {
    SimpleSpellChecker sp;

    @Before
    public void setup() {
        try {
            sp = new SimpleSpellChecker(new Dictionary(), "12apple 23apple i love you you love 23 i");
        } catch (InvalidDataFormatException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testGetMisspelledWords() {
        assertEquals(Arrays.asList("23","23apple","12apple"), sp.getMisspelledWords());
    }

    @Test
    public void testGetUniqueWords(){
        assertEquals(Arrays.asList("love", "23","23apple","12apple","i","you"),sp.getUniqueWords());
    }

    @Test
    public void testGetFrequencyOfWord (){

        try {
            assertEquals(1,sp.getFrequencyOfWord("12apple"));
            assertEquals(1,sp.getFrequencyOfWord("23apple"));
            assertEquals(2,sp.getFrequencyOfWord("you"));
            assertEquals(2,sp.getFrequencyOfWord("love"));
            assertEquals(2,sp.getFrequencyOfWord("i"));
        } catch (InvalidDataFormatException e) {
            e.printStackTrace();
        }
    }

}