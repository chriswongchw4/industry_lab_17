package ictgradschool.industry.lab17.ex02;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * TODO Implement this class.
 */
public class TestDictionary {
    Dictionary dictionary;

    @Before
    public void setUp() {
       dictionary = new Dictionary();
    }


    @Test
    public void testIsSpellCorrect() {
        assertTrue(dictionary.isSpellingCorrect("risk"));
        assertTrue(dictionary.isSpellingCorrect("smile"));
        assertTrue(dictionary.isSpellingCorrect("nuclear"));
        assertFalse(dictionary.isSpellingCorrect("sdfsdfsdfs"));
        assertFalse(dictionary.isSpellingCorrect("123456789"));
        assertFalse(dictionary.isSpellingCorrect("cameron"));
    }
}