package ictgradschool.industry.lab17.ex01;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class RobotTest {

    private Robot myRobot;

    @Before
    public void setUp() {
        myRobot = new Robot();
    }

    @Test
    public void testRobotConstruction() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());
    }

    @Test
    public void testIllegalMoveNorth() {
        boolean atTop = false;
        try {
            // Move the robot to the top row.
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++) {
                myRobot.move();
            }

            // Check that robot has reached the top.
            atTop = myRobot.currentState().row == 1;
            assertTrue(atTop);
        } catch (IllegalMoveException e) {
            fail();
        }

        try {
            // Now try to continue to move North.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(1, myRobot.currentState().row);
        }
    }

    @Test
    public void testTurnFromWestToNorth() {

        for (int i = 0; i < 4; i++) {
            if (myRobot.currentState().direction == Robot.Direction.West) {
                int originalRow = myRobot.currentState().row;
                int originalColumn = myRobot.currentState().column;
                myRobot.turn();
                assertEquals(Robot.Direction.North, myRobot.currentState().direction);
                assertEquals(originalRow, myRobot.currentState().row);
                assertEquals(originalColumn, myRobot.currentState().column);
            }
        }
    }

    @Test
    public void testTurnFromEastToSouth() {
        for (int i = 0; i < 4; i++) {
            if (myRobot.currentState().direction == Robot.Direction.East) {
                int originalRow = myRobot.currentState().row;
                int originalColumn = myRobot.currentState().column;
                myRobot.turn();
                assertEquals(Robot.Direction.South, myRobot.currentState().direction);
                assertEquals(originalRow, myRobot.currentState().row);
                assertEquals(originalColumn, myRobot.currentState().column);
            }
        }
    }

    @Test
    public void testTurnFromSouthToWest() {

        for (int i = 0; i < 4; i++) {
            if (myRobot.currentState().direction == Robot.Direction.South) {
                int originalRow = myRobot.currentState().row;
                int originalColumn = myRobot.currentState().column;
                myRobot.turn();
                assertEquals(Robot.Direction.West, myRobot.currentState().direction);
                assertEquals(originalRow, myRobot.currentState().row);
                assertEquals(originalColumn, myRobot.currentState().column);
            }
        }
    }

    @Test
    public void testTurnFromNorthToEast() {
        for (int i = 0; i < 4; i++) {
            if (myRobot.currentState().direction == Robot.Direction.North) {
                int originalRow = myRobot.currentState().row;
                int originalColumn = myRobot.currentState().column;
                myRobot.turn();
                assertEquals(Robot.Direction.East, myRobot.currentState().direction);
                assertEquals(originalRow, myRobot.currentState().row);
                assertEquals(originalColumn, myRobot.currentState().column);
            }
        }
    }

    @Test
    public void testMoveNorth() throws IllegalMoveException {
        if (myRobot.currentState().direction == Robot.Direction.North) {
            int originalRow = myRobot.currentState().row;
            myRobot.move();
            assertTrue(myRobot.currentState().row == originalRow - 1);
        }
    }

    @Test
    public void testMoveEast() throws IllegalMoveException {
        myRobot.turn(); //turn from north to East before the test.
        if (myRobot.currentState().direction == Robot.Direction.East) {
            int originalColumn = myRobot.currentState().column;
            myRobot.move();
            assertTrue(myRobot.currentState().column == originalColumn + 1);
        }
    }

    @Test
    public void testMoveSouth() throws IllegalMoveException {
        myRobot.move();
        myRobot.turn();
        myRobot.move();
        myRobot.turn(); //turn from north to south before the test.
        if (myRobot.currentState().direction == Robot.Direction.South) {
            int originalRow = myRobot.currentState().row;
            myRobot.move();
            assertTrue(myRobot.currentState().row == originalRow + 1);
        }
    }

    @Test
    public void testMoveWest() throws IllegalMoveException {

        myRobot.move();
        myRobot.turn();
        myRobot.move();
        myRobot.turn();
        myRobot.turn(); //move away from the original location and turn from north to west before the test.

        if (myRobot.currentState().direction == Robot.Direction.West) {
            int originalColumn = myRobot.currentState().column;
            myRobot.move();
            assertTrue(myRobot.currentState().column == originalColumn - 1);
        }
    }

    @Test
    public void testIllegalMoveSouth() {
        myRobot.turn();
        myRobot.turn(); // turn the robot facing south
        try {
            // Now try to move South.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(10, myRobot.currentState().row);
        }
    }

    @Test
    public void testIllegalMoveWest() {
        myRobot.turn();
        myRobot.turn();
        myRobot.turn(); // turn the robot facing West
        try {
            // Now try to move west.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(1, myRobot.currentState().column);
        }
    }

    @Test
    public void testIllegalMoveEast() {
        boolean atRight = false;
        try {
            myRobot.turn();// turn to face east
            // Move the robot to the top row.
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++) {
                myRobot.move();
            }

            // Check that robot has reached the top.
            atRight = myRobot.currentState().column == 10;
            assertTrue(atRight);
        } catch (IllegalMoveException e) {
            fail();
        }

        try {
            // Now try to continue to move North.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(10, myRobot.currentState().column);
        }
    }


}
